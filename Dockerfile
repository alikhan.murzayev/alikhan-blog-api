FROM alpine:3.9

ADD target/app /go/bin/app
ADD src/config src/config

ENTRYPOINT ["/go/bin/app"]
