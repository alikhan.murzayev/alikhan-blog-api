package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/alikhan.murzayev/alikhan-blog-api/src/blog"
	"gitlab.com/alikhan.murzayev/alikhan-blog-api/src/config"
	"gitlab.com/alikhan.murzayev/alikhan-blog-api/src/repository/elastic"
	"gitlab.com/alikhan.murzayev/alikhan-blog-api/src/repository/scylladb"
)

func main() {
	httpPort := flag.String("http.port", ":8080", "HTTP listen address only port :8080")
	flag.Parse()

	// Load configs
	err := config.GetConfigs()
	if err != nil {
		fmt.Println("Error occurred while loading configs")
		panic(err)
	}

	// Init connections
	err = scylladb.ScyllaDBCreateTables()
	if err != nil {
		fmt.Println("Error occurred while creating tables")
		fmt.Println(err.Error())
	}

	scyllaDBConnection, err := scylladb.ScyllaDBConnectionStart()
	if err != nil {
		fmt.Println("Error occurred while connecting to ScyllaDB")
		panic(err)
	}

	elasticConnection, err := elastic.ElasticConnectionStart()
	if err != nil {
		fmt.Println("Error occurred while connection to Elasticsearch")
		panic(err)
	}

	defer scyllaDBConnection.Close()

	postCommandRepo := scylladb.NewPostCommandRepo(scyllaDBConnection)
	postQueryRepo := elastic.NewPostQueryRepo(elasticConnection)

	// Init blog service
	blogService := blog.NewService(postCommandRepo, postQueryRepo)

	// Init http routes
	mux := http.NewServeMux()
	mux.Handle("/blog-api/", blog.MakeHandler(blogService))
	http.Handle("/blog-api/", accessControl(mux))
	http.HandleFunc("/check", healthChecks)

	errs := make(chan error, 1)

	// Init http serve
	go func() {
		fmt.Printf("Listening blog-api at port %s\n", *httpPort)
		errs <- http.ListenAndServe(*httpPort, nil)
	}()

	// Lister errors chan
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT)
		errs <- fmt.Errorf("%s", <-c)
	}()

	fmt.Println("terminated", <-errs)

}

// Additional functions and structures

func healthChecks(w http.ResponseWriter, r *http.Request) {
	_, _ = fmt.Fprintf(w, "ok")
}

func accessControl(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type, Authorization,X-Owner,darvis-dialog-id")

		if r.Method == "OPTIONS" {
			return
		}

		h.ServeHTTP(w, r)
	})
}
