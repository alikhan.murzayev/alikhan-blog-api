## Run app in docker

### Create network
docker network create alikhan-blog

### Run ScyllaDB
docker run -d --name blog-scylla -p 9042:9042 --network alikhan-blog scylladb/scylla

### Run Elasticsearch
docker run -d --name blog-elastic -p 9200:9200 -p 9300:9300 --network alikhan-blog -e "discovery.type=single-node" elasticsearch:7.6.1

### Run app 
docker run -d --name alikhan-blog-api -p 8080:8080 --network alikhan-blog --env config=config-dev.json 73d702318d0d
