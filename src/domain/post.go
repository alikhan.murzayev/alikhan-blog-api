package domain

import (
	"github.com/google/uuid"
)

// Main structure & methods
type Post struct {
	Uid              string   `json:"uid,omitempty"`
	CategoryId       string   `json:"category_id"`
	AuthorId         string   `json:"author_id"`
	Title            string   `json:"title"`
	ShortDescription string   `json:"short_description,omitempty"`
	PreviewUrl       string   `json:"preview_url,omitempty"`
	Content          string   `json:"content"`
	Tags             []string `json:"tags,omitempty"`
}

// Generate blog unique ID
func (p *Post) GenerateUid() {
	p.Uid = uuid.New().String()
}

// Repository interfaces
type PostCommandRepo interface {
	Store(post *Post) error
	GetByID(id string) (*Post, error)
	DeleteById(id string) error
}

type PostQueryRepo interface {
	Store(post *Post) error
	GetByID(id string) (*Post, error)
	DeleteById(id string) error
	GetByFilters(filter interface{}) (*[]Post, error)
}

// Structure for filtering
type Filter struct {
	CategoryIds []string `json:"category_ids,omitempty"`
	AuthorIds   []string `json:"author_ids,omitempty"`
	Titles      []string `json:"titles,omitempty"`
	Tags        []string `json:"tags,omitempty"`
}
