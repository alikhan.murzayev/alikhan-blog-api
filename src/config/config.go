package config

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

// Global variable that stores all configs
var AllConfigs Configs

// Load all configs into global variable AllConfigs
func GetConfigs() error {
	var filePath string

	if os.Getenv("config") != "" {
		currentDir, err := os.Getwd()
		if err != nil {
			return err
		}

		filePath = currentDir + "/src/config/" + os.Getenv("config")
	} else {
		currentDir, err := os.Getwd()
		if err != nil {
			return err
		}

		filePath = currentDir + "/src/config/config.json"
	}

	data, err := ioutil.ReadFile(filePath)
	if err != nil {
		return err
	}

	err = json.Unmarshal(data, &AllConfigs)
	if err != nil {
		return err
	}
	return nil
}

type Configs struct {
	ScyllaDB ScyllaDBConfig `json:"scylla_db"`
	Elastic  ElasticConfig  `json:"elastic"`
}

type ScyllaDBConfig struct {
	ConnectionIp []string `json:"connection_ip"`
	KeySpace     string   `json:"key_space"`
}

type ElasticConfig struct {
	ConnectionUrl []string `json:"connection_url"`
}
