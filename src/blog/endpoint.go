package blog

import (
	"context"
	"github.com/go-kit/kit/endpoint"

	"gitlab.com/alikhan.murzayev/alikhan-blog-api/src/domain"
)

// Blog endpoint

// Create/Update blog req & rest
type SavePostRequest struct {
	domain.Post
}

type SavePostResponse struct {
	PostId string `json:"post_id"`
}

// Save post endpoint
func makeSavePostEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(SavePostRequest)
		resp, err := s.SavePost(&req)
		if err != nil {
			return nil, err
		}

		return resp, nil
	}
}

// Get post req & resp
type GetPostRequest struct {
	PostId string `json:"post_id"`
}

type GetPostResponse struct {
	domain.Post
}

// Get post endpoint
func makeGetPostEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetPostRequest)
		resp, err := s.GetPost(&req)
		if err != nil {
			return nil, err
		}

		return resp, nil
	}
}

// Delete post req & resp
type DeletePostRequest struct {
	PostId string `json:"post_id"`
}

type DeletePostResponse struct {
	Msg string `json:"msg"`
}

// Delete post endpoint
func makeDeletePostEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(DeletePostRequest)
		resp, err := s.DeletePost(&req)
		if err != nil {
			return nil, err
		}

		return resp, nil
	}
}

// Filter post req & resp
type FilterPostRequest struct {
	CategoryIds []string `json:"category_ids,omitempty"`
	AuthorIds   []string `json:"author_ids,omitempty"`
	Titles      []string `json:"titles,omitempty"`
	Tags        []string `json:"tags,omitempty"`
}

type FilterPostResponse struct {
	Posts []domain.Post `json:"posts"`
}

// Filter post endpoint
func makeFilterPostEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(FilterPostRequest)
		resp, err := s.GetByFilters(&req)
		if err != nil {
			return nil, err
		}

		return resp, nil
	}
}
