package blog

import (
	"fmt"
	"sync"

	"gitlab.com/alikhan.murzayev/alikhan-blog-api/src/domain"
)

type Service interface {
	SavePost(req *SavePostRequest) (*SavePostResponse, error)
	GetPost(req *GetPostRequest) (*GetPostResponse, error)
	DeletePost(req *DeletePostRequest) (*DeletePostResponse, error)
	GetByFilters(req *FilterPostRequest) (*FilterPostResponse, error)
}

type service struct {
	postCommandRepo domain.PostCommandRepo
	postQueryRepo   domain.PostQueryRepo
}

func NewService(postCR domain.PostCommandRepo, postQR domain.PostQueryRepo) Service {
	return &service{
		postCommandRepo: postCR,
		postQueryRepo:   postQR,
	}
}

// Blog service methods

// Create/Update blog service methods
func (s *service) SavePost(req *SavePostRequest) (*SavePostResponse, error) {

	post := req.Post

	if post.Uid == "" {
		post.GenerateUid()
	}

	errChan := make(chan error, 2)
	waitGroup := sync.WaitGroup{}

	// Save to ScyllaDB
	waitGroup.Add(1)
	go func(wg *sync.WaitGroup, post *domain.Post, errChan chan<- error, repo domain.PostCommandRepo) {
		defer wg.Done()

		err := repo.Store(post)
		if err != nil {
			errChan <- err
			return
		}
	}(&waitGroup, &post, errChan, s.postCommandRepo)

	// Save to Elasticsearch
	waitGroup.Add(1)
	go func(wg *sync.WaitGroup, post *domain.Post, errChan chan<- error, repo domain.PostQueryRepo) {
		defer wg.Done()

		err := repo.Store(post)
		if err != nil {
			errChan <- err
			return
		}
	}(&waitGroup, &post, errChan, s.postQueryRepo)

	waitGroup.Wait()
	close(errChan)
	err, exists := <-errChan
	if exists {
		return nil, err
	}

	return &SavePostResponse{PostId: post.Uid}, nil
}

// Get post service method
func (s *service) GetPost(req *GetPostRequest) (*GetPostResponse, error) {
	post, err := s.postQueryRepo.GetByID(req.PostId)
	if err != nil {
		return nil, err
	}

	return &GetPostResponse{*post}, nil
}

// Delete Post service method
func (s *service) DeletePost(req *DeletePostRequest) (*DeletePostResponse, error) {

	errChan := make(chan error, 2)
	waitGroup := sync.WaitGroup{}

	// Delete from ScyllaDB
	waitGroup.Add(1)
	go func(wg *sync.WaitGroup, errChan chan<- error, repo domain.PostCommandRepo, id string) {
		defer wg.Done()

		err := repo.DeleteById(id)
		if err != nil {
			errChan <- err
			return
		}
	}(&waitGroup, errChan, s.postCommandRepo, req.PostId)

	// Delete from Elasticsearch
	waitGroup.Add(1)
	go func(wg *sync.WaitGroup, errChan chan<- error, repo domain.PostQueryRepo, id string) {
		defer wg.Done()

		err := repo.DeleteById(id)
		if err != nil {
			errChan <- err
			return
		}

	}(&waitGroup, errChan, s.postQueryRepo, req.PostId)

	waitGroup.Wait()
	close(errChan)
	err, exists := <-errChan
	if exists {
		return nil, err
	}

	return &DeletePostResponse{Msg: fmt.Sprintf("post deleted successfully")}, nil
}

// Filter post service methods
func (s *service) GetByFilters(req *FilterPostRequest) (*FilterPostResponse, error) {
	posts, err := s.postQueryRepo.GetByFilters(*req)
	if err != nil {
		return nil, err
	}

	return &FilterPostResponse{Posts: *posts}, nil
}
