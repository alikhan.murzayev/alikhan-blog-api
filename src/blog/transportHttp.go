package blog

import (
	"context"
	"encoding/json"
	"errors"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"net/http"
)

// Http routes handlers

func MakeHandler(s Service) http.Handler {

	// Options array
	options := []kithttp.ServerOption{
		kithttp.ServerErrorEncoder(encodeError),
	}

	// Blog routes handlers
	savePost := kithttp.NewServer(
		makeSavePostEndpoint(s),
		decodeSavePostRequest,
		encodeResponse,
		options...)

	getPost := kithttp.NewServer(
		makeGetPostEndpoint(s),
		decodeGetPostRequest,
		encodeResponse,
		options...)

	deletePost := kithttp.NewServer(
		makeDeletePostEndpoint(s),
		decodeDeletePostRequest,
		encodeResponse,
		options...)

	filterPost := kithttp.NewServer(
		makeFilterPostEndpoint(s),
		decodeFilterPostRequest,
		encodeResponse,
		options...)

	// Init routes
	r := mux.NewRouter()

	// Blog routes
	r.Handle("/blog-api/post", savePost).Methods("PUT", "POST")
	r.Handle("/blog-api/post/{id}", getPost).Methods("GET")
	r.Handle("/blog-api/post/filter", filterPost).Methods("POST")
	r.Handle("/blog-api/post/{id}", deletePost).Methods("DELETE")

	return r
}

// Blog request decoders

// Create/Update blog request decoder
func decodeSavePostRequest(_ context.Context, r *http.Request) (interface{}, error) {

	var body SavePostRequest

	// Parse request
	err := json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		return nil, err
	}

	// Validate request
	if body.CategoryId == "" {
		return nil, errors.New("category id was not provided")
	}
	if body.AuthorId == "" {
		return nil, errors.New("author id was not provided")
	}
	if body.Title == "" {
		return nil, errors.New("title was not provided")
	}
	if body.Content == "" {
		return nil, errors.New("content was not provided")
	}

	return body, nil
}

// Get post request decoder
func decodeGetPostRequest(_ context.Context, r *http.Request) (interface{}, error) {

	var body GetPostRequest
	vars := mux.Vars(r)

	id, ok := vars["id"]
	if !ok {
		return nil, errors.New("post id was not provided")
	}

	body.PostId = id

	return body, nil
}

// Delete post request decoder
func decodeDeletePostRequest(_ context.Context, r *http.Request) (interface{}, error) {

	var body DeletePostRequest
	vars := mux.Vars(r)

	id, ok := vars["id"]
	if !ok {
		return nil, errors.New("post id was not provided")
	}

	body.PostId = id

	return body, nil
}

// Filter post request decoder
func decodeFilterPostRequest(_ context.Context, r *http.Request) (interface{}, error) {

	var body FilterPostRequest
	err := json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		return nil, err
	}

	return body, nil
}

// Blog request encoders
func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(errorer); ok && e.error() != nil {
		encodeError(ctx, e.error(), w)
		return nil
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}

// Error response encoder
type errorer interface {
	error() error
}

func encodeError(_ context.Context, err error, w http.ResponseWriter) {
	w.Header().Set("Content-Type", "text/plain; charset=utf-8")
	_, _ = w.Write([]byte(err.Error()))
}
