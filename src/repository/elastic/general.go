package elastic

import (
	"github.com/olivere/elastic/v7"

	"gitlab.com/alikhan.murzayev/alikhan-blog-api/src/config"
)

// Init Elastic connection
func ElasticConnectionStart() (*elastic.Client, error) {
	client, err := elastic.NewClient(elastic.SetURL(config.AllConfigs.Elastic.ConnectionUrl...), elastic.SetSniff(true))
	if err != nil {
		return nil, err
	}

	return client, nil
}
