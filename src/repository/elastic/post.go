package elastic

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/olivere/elastic/v7"

	"gitlab.com/alikhan.murzayev/alikhan-blog-api/src/blog"
	"gitlab.com/alikhan.murzayev/alikhan-blog-api/src/domain"
)

type postQueryRepo struct {
	client *elastic.Client
}

func NewPostQueryRepo(client *elastic.Client) domain.PostQueryRepo {
	return &postQueryRepo{client: client}
}

func (postQR *postQueryRepo) Store(post *domain.Post) error {
	_, err := postQR.client.Index().
		Index("blog").
		Type("post").
		Id(post.Uid).
		BodyJson(post).
		Refresh("true").
		Do(context.TODO())
	if err != nil {
		return err
	}
	return nil
}

func (postQR *postQueryRepo) GetByID(id string) (*domain.Post, error) {

	var post domain.Post

	get, err := postQR.client.Get().Index("blog").Type("post").Id(id).Do(context.TODO())
	if err != nil {
		return nil, err
	}

	if get.Found {
		err = json.Unmarshal(get.Source, &post)
		if err != nil {
			return nil, err
		}

		return &post, nil
	} else {
		return nil, errors.New("not found")
	}
}

func (postQR *postQueryRepo) DeleteById(id string) error {
	_, err := postQR.client.Delete().Index("blog").Type("post").Id(id).Do(context.TODO())
	if err != nil {
		return err
	}

	return nil
}

func (postQR *postQueryRepo) GetByFilters(filter interface{}) (*[]domain.Post, error) {

	query := elastic.NewBoolQuery()

	filterReq, ok := filter.(blog.FilterPostRequest)
	if !ok {
		return nil, errors.New("wrong filter is provided")
	}

	// Filter by tags
	queryTags := elastic.NewBoolQuery()
	for i := range filterReq.Tags {
		termQuery := elastic.NewTermQuery("tags", filterReq.Tags[i])
		queryTags = queryTags.Should(termQuery)
	}

	// Filter by authors
	queryAuthorIds := elastic.NewBoolQuery()
	for i := range filterReq.AuthorIds {
		termQuery := elastic.NewTermQuery("author_id", filterReq.AuthorIds[i])
		queryAuthorIds = queryAuthorIds.Should(termQuery)
	}

	// Filter by categories
	queryCategoryIds := elastic.NewBoolQuery()
	for i := range filterReq.CategoryIds {
		termQuery := elastic.NewTermQuery("category_id", filterReq.CategoryIds[i])
		queryCategoryIds = queryCategoryIds.Should(termQuery)
	}

	query = query.Filter(queryTags, queryAuthorIds, queryCategoryIds)

	searchResult, err := postQR.client.Search().
		Index("blog").
		Query(query).
		Pretty(true).
		Do(context.TODO())

	if err != nil {
		return nil, err
	}

	var posts []domain.Post

	for _, hit := range searchResult.Hits.Hits {
		var post domain.Post
		err := json.Unmarshal(hit.Source, &post)
		if err != nil {
			fmt.Println("Deserialization error")
		}

		posts = append(posts, post)
	}

	return &posts, nil
}
