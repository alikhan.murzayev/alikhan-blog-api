package scylladb

import (
	"encoding/json"
	"github.com/gocql/gocql"

	"gitlab.com/alikhan.murzayev/alikhan-blog-api/src/domain"
)

type postCommandRepo struct {
	dbSession *gocql.Session
}

func NewPostCommandRepo(session *gocql.Session) domain.PostCommandRepo {
	return &postCommandRepo{dbSession: session}
}

// Store blog to ScyllaDB
func (r *postCommandRepo) Store(post *domain.Post) error {
	postJson, err := json.Marshal(post)
	if err != nil {
		return err
	}

	err = r.dbSession.Query("INSERT INTO posts (id, payload) VALUES (?, ?)",
		post.Uid,
		string(postJson)).
		Exec()
	if err != nil {
		return err
	}

	return nil
}

// Get blog by id from ScyllaDB
func (r *postCommandRepo) GetByID(id string) (*domain.Post, error) {
	var postData string
	var post domain.Post

	err := r.dbSession.Query("SELECT payload FROM posts WHERE id = ?", id).Scan(&postData)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal([]byte(postData), &post)
	if err != nil {
		return nil, err
	}

	return &post, nil
}

// Delete blog by id from ScyllaDB
func (r *postCommandRepo) DeleteById(id string) error {
	err := r.dbSession.Query("DELETE FROM posts WHERE id = ?", id).Exec()
	if err != nil {
		return err
	}

	return nil
}
