package scylladb

import (
	"fmt"
	"github.com/gocql/gocql"
	"time"

	"gitlab.com/alikhan.murzayev/alikhan-blog-api/src/config"
)

// Init ScyllaDB connection
func ScyllaDBConnectionStart() (*gocql.Session, error) {
	cluster := gocql.NewCluster(config.AllConfigs.ScyllaDB.ConnectionIp...)
	cluster.Consistency = gocql.Quorum
	cluster.Keyspace = config.AllConfigs.ScyllaDB.KeySpace
	cluster.ProtoVersion = 4
	cluster.SocketKeepalive = 10 * time.Second

	client, err := cluster.CreateSession()
	if err != nil {
		return nil, err
	}

	return client, nil
}

func ScyllaDBCreateTables() error {
	cluster := gocql.NewCluster(config.AllConfigs.ScyllaDB.ConnectionIp...)
	cluster.Consistency = gocql.Quorum
	cluster.ProtoVersion = 4
	cluster.SocketKeepalive = 10 * time.Second

	client, err := cluster.CreateSession()
	if err != nil {
		return err
	}

	defer client.Close()

	stmt := `
		CREATE KEYSPACE %s
		WITH replication = {
			'class' : 'SimpleStrategy',
			'replication_factor' : 1
		};`
	stmt = fmt.Sprintf(stmt, config.AllConfigs.ScyllaDB.KeySpace)
	err = client.Query(stmt).Exec()
	if err != nil {
		return err
	}

	stmt = `
		CREATE TABLE %s.posts (
			id uuid,
			payload text,
			PRIMARY KEY (id)
		);`
	stmt = fmt.Sprintf(stmt, config.AllConfigs.ScyllaDB.KeySpace)
	err = client.Query(stmt).Exec()
	if err != nil {
		return err
	}

	return nil
}
